<?php 

  require 'function.php';

  session_start();

  if (isset($_SESSION["login"])){
    $id = $_SESSION['id'];
    $access = mysqli_query($conn, "SELECT access FROM user WHERE id = $id");
    $user = mysqli_fetch_assoc($access);

    if($user["access"] === "admin"){
      header("Location: admin/index.php");
    }else{
      header("Location: user/index.php");
    }
    exit;
}
  
  $isi = query("SELECT * FROM anggota");
  
?>
<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Ganesha Pronity</title>

    <!-- Bootstrap -->
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/style.css">
    <?= $icon ?>

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>
    <!-- Navbar -->
    <nav class="navbar navbar-inverse navbar-fixed-top">
      <div class="container-fluid">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>

          <a href="#home" class="navbar-brand page-scroll">Ganesa Pronity</a>
        </div>

        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
          <ul class="nav navbar-nav navbar-right">
            <li><a href="#about" class="page-scroll">About</a></li>
            <li><a href="#repo" class="page-scroll">Repository</a></li>
            <li><a href="#contact" class="page-scroll">Contact</a></li>
            <li><a href="login.php">Login</a></li>
            <li><a href="registrasi.php">Sigh in</a></li>
          </ul>
        </div>
      </div>
    </nav>
    <!-- Akhir Navbar -->
    
    <!-- Jumbotron -->
    <div class="jumbotron text-center">
        <br>
        <img src="img/GP.png" alt="icon-GP" class="img-circle">
        <h1 class="judul">Ganesha Programing Community</h1>
        <i class="glyphicon glyphicon-star"></i><i class="glyphicon glyphicon-star"></i><i class="glyphicon glyphicon-star"></i>
        <h2 class="tulisan"> </h2>
        <div class="container">
          <div class="row">
            <a href="#about"><div class="col-xs-3 col-xs-offset-2"> About Us</div></a>
            <a href="registrasi.php"><div class="col-xs-3 col-xs-offset-2"> Join Us</div></a>
          </div>
        </div>
    </div>
    <!-- End Jumbotron -->
    
    <!-- About -->
    <div class="container-fluid text-center" id="about">
        <div class="row">
            <div class="paragraf">
                <h2>About</h2>
                <hr>
                <p class="text-justify">The Ganesha programming community is the place to hone your programming skills. There are so many things that can be learned here, one of which is web programming. Here we will help you even from scratch. So what are you waiting for, join us!</p>
                <p class="text-justify">There are several houses that you can choose from, including:</p>
                <dl>
                    <dt>Gryffindor</dt>
                    <dd class="text-justify">Lorem ipsum dolor sit, amet consectetur adipisicing elit. Dolor vel magni corrupti, molestiae esse vitae sequi velit animi voluptas perferendis, architecto illum labore eos eveniet nisi. Fuga consequatur aliquam quas. Lorem ipsum, dolor sit amet consectetur adipisicing elit. Tempora, doloremque suscipit ducimus voluptatum sit quaerat natus quis pariatur sed earum placeat hic mollitia eligendi. Repudiandae magni rerum illum soluta! Obcaecati. Lorem ipsum dolor sit amet consectetur adipisicing elit. At sequi cum, inventore vel voluptates suscipit sapiente fugiat? Repudiandae nam eaque perspiciatis vero, quaerat nisi minima, esse asperiores explicabo dolores ipsa.</dd>

                    <dt>Hufflepuff</dt>
                    <dd class="text-justify">Lorem ipsum dolor sit, amet consectetur adipisicing elit. Dolor vel magni corrupti, molestiae esse vitae sequi velit animi voluptas perferendis, architecto illum labore eos eveniet nisi. Fuga consequatur aliquam quas. Lorem ipsum, dolor sit amet consectetur adipisicing elit. Tempora, doloremque suscipit ducimus voluptatum sit quaerat natus quis pariatur sed earum placeat hic mollitia eligendi. Repudiandae magni rerum illum soluta! Obcaecati. Lorem ipsum dolor sit amet consectetur adipisicing elit. At sequi cum, inventore vel voluptates suscipit sapiente fugiat? Repudiandae nam eaque perspiciatis vero, quaerat nisi minima, esse asperiores explicabo dolores ipsa.</dd>

                    <dt>Revenclaw</dt>
                    <dd class="text-justify">Lorem ipsum dolor sit, amet consectetur adipisicing elit. Dolor vel magni corrupti, molestiae esse vitae sequi velit animi voluptas perferendis, architecto illum labore eos eveniet nisi. Fuga consequatur aliquam quas. Lorem ipsum, dolor sit amet consectetur adipisicing elit. Tempora, doloremque suscipit ducimus voluptatum sit quaerat natus quis pariatur sed earum placeat hic mollitia eligendi. Repudiandae magni rerum illum soluta! Obcaecati. Lorem ipsum dolor sit amet consectetur adipisicing elit. At sequi cum, inventore vel voluptates suscipit sapiente fugiat? Repudiandae nam eaque perspiciatis vero, quaerat nisi minima, esse asperiores explicabo dolores ipsa.</dd>

                    <dt>Slytherin</dt>
                    <dd class="text-justify">Lorem ipsum dolor sit, amet consectetur adipisicing elit. Dolor vel magni corrupti, molestiae esse vitae sequi velit animi voluptas perferendis, architecto illum labore eos eveniet nisi. Fuga consequatur aliquam quas. Lorem ipsum, dolor sit amet consectetur adipisicing elit. Tempora, doloremque suscipit ducimus voluptatum sit quaerat natus quis pariatur sed earum placeat hic mollitia eligendi. Repudiandae magni rerum illum soluta! Obcaecati. Lorem ipsum dolor sit amet consectetur adipisicing elit. At sequi cum, inventore vel voluptates suscipit sapiente fugiat? Repudiandae nam eaque perspiciatis vero, quaerat nisi minima, esse asperiores explicabo dolores ipsa.</dd>
                </dl>
            </div>
        </div>
    </div>
    <!-- End About -->

    <!-- Galery -->
    <!-- End of galery -->

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="js/jquery-3.5.1.min.js"></script>
    <script src="js/jquery.easing.1.3.js"></script>
    <!-- <script src="https://code.jquery.com/jquery-1.12.4.min.js" integrity="sha384-nvAa0+6Qg9clwYCGGPpDQLVpLNn0fRaROjHqs13t4Ggj3Ez50XnGQqc/r8MhnRDZ" crossorigin="anonymous"></script> -->
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js" integrity="sha384-aJ21OjlMXNL5UyIl/XNwTMqvzeRMZH2w8c5cRVpzpU8Y5bApTppSuUkhZXN0VxHd" crossorigin="anonymous"></script>
  </body>
</html>