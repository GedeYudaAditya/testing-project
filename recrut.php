<?php 
    require 'function.php';

    // $user = mysqli_query($conn, "SELECT * FROM user WHERE username = '$username';");
    session_start();

    if(isset($_SESSION['id'])){
        $id = $_SESSION['id'];

        $akun = mysqli_query($conn, "SELECT id FROM anggota WHERE id = $id");

        if(mysqli_affected_rows($conn) === 1){
            header("Location: index.php");
            exit;
        }
    }

    if(!isset($_SESSION["login"])){
        header("Location: login.php");
        exit;
    }

    if(isset($_POST["kirim"])){

        if($isi = rekrut($_POST) > 0){
            echo "<script>
                    alert('Selamat, Anda Telah Bergabung!');
                    document.location.href = 'index.php'
                  </script>";



                // $masuk = mysqli_fetch_assoc($user);

                // if($masuk["access"] === "admin"){
                //     echo "<script>
                //             document.location.href = 'admin/index.php'
                //           </script>";
                // }else{
                //     echo "<script>
                //             document.location.href = 'user/index.php'
                //           </script>";
                // }
        }else{
            var_dump($isi);
            die;
            echo "<script>
                    alert('Error : Penambahan Anggota Gagal!');
                    document.location.href = 'recrut.php'
                  </script>";
        }
    }
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Recrutment</title>
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/regist.css">
    <?= $icon ?>
</head>
<body>
    <div class="semua">
    <div class="judul"><h1 class="text-center">Recrutment New Member</h1></div>
    <div class="container">
        <div class="row">
            <div class="col-sm-4 col-sm-offset-4 kotak">
            <div class="col-sm-12 text-center">
                <form action="" method="POST">
                    <label for="username">Nama</label>
                    <br>
                    <input type="text" name="nama" id="username" placeholder="Massukkan Nama" required>
                    <br>
                    <label for="email">NIM</label>
                    <br>
                    <input type="text" name="nim" id="email" placeholder="Massukkan NIM" required>
                    <br>
                    <label for="password">Alamat</label>
                    <br>
                    <input type="text" name="alamat" id="password" placeholder="Masukkan Alamat" required>
                    <br>
                    <label for="house">Houses</label>
                    <br>
                    <select name="house" id="house">
                        <option value="">--None--</option>
                        <option value="Gryffindor">Gryffindor</option>
                        <option value="Hufflepuff">Hufflepuff</option>
                        <option value="Revenclaw">Revenclaw</option>
                        <option value="Slytherin">Slytherin</option>
                    </select>
                    <br>
                    <button type="submit" name="kirim" class="btn btn-primary">Kirim</button>
                </form>
            </div>
            </div>
        </div>
    </div>
    </div>
    <footer class="text-center">
        <p>&copy; 2021, create by Yuda aditya.</p>
    </footer>
</body>
</html>