<?php 
    require 'function.php';

    session_start();

    if (isset($_SESSION["login"])){
        header("Location: index.php");
        exit;
    }

    if(isset($_POST["login"])){

        $username = $_POST["username"];
        $password = $_POST["password"];

        $user = mysqli_query($conn, "SELECT * FROM user WHERE username = '$username';");

        if(mysqli_affected_rows($conn) === 1){

            $masuk = mysqli_fetch_assoc($user);
            // var_dump($masuk);

            if(password_verify($password, $masuk["password"])){
                if($masuk["access"] === "admin"){
                    $_SESSION["login"] = true;
                    $_SESSION["id"] = $masuk["id"];
                    header("Location: admin/index.php");
                }else{
                    $_SESSION["login"] = true;
                    $_SESSION["id"] = $masuk["id"];
                    header("Location: user/index.php");
                }
                exit;
            }
            
        }
        $error = true;
    }
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Registrasi</title>
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/regist.css">
    <?= $icon ?>
</head>
<body>
    <div class="semua">
    <div class="judul"><h1 class="text-center">Login</h1></div>
    <div class="container">
        <div class="row">
            <div class="col-sm-4 col-sm-offset-4 kotak">
            <?php if(isset($error)) :?>
                <p style="color: red; font-weight: 600; font-style:italic;" class="text-center">Username atau Password Salah!</p>
            <?php endif;?>
            <div class="col-sm-12 text-center">
                <form action="" method="POST">
                    <label for="username">Username</label>
                    <br>
                    <input type="text" name="username" id="username" placeholder="Massukkan Username">
                    <br>
                    <label for="password">Password</label>
                    <br>
                    <input type="password" name="password" id="password" placeholder="Massukkan Password">
                    <br>
                    <button type="submit" name="login" class="btn btn-primary">Login</button>
                </form>

                <p>Belum Punya Akun ? <a href="registrasi.php">Registrasi</a> dulu!</p>
            </div>
            </div>
        </div>
    </div>
    </div>
    <footer class="text-center">
        <p>&copy; 2021, create by Yuda aditya.</p>
    </footer>
</body>
</html>